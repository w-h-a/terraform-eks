module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.24.1"

  cluster_name = "myapp-eks"
  cluster_version = "1.22"

  subnet_ids = module.myapp_vpc.private_subnets
  vpc_id = module.myapp_vpc.vpc_id

  tags = {
    environment = "dev"
    application = "myapp"
  }

  eks_managed_node_groups = {
    dev = {
        min_size = 3
        max_size = 6
        desired_size = 3

        instance_types = ["t2.small"]
    }
  }
}